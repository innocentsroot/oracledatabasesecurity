@extends('layouts.Semantic.auth')
@section("content")
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui teal image header">
                <div class="content">Log-in to your account</div>
            </h2>
            <form class="ui large form {{$errors==[] ? 'no-error' : 'error'}}" method="POST" action="{{route('login')}}">
                @csrf
                <div class="ui stacked segment">
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" placeholder="E-mail address">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                    <button class="ui fluid large teal submit button" type="submit">Login</button>
                </div>
                <div class="ui error message">
                    @if ($errors->has('email'))
                        <p>{{ $errors->first('email') }}</p>
                    @endif
                    @if ($errors->has('password'))
                        <p>{{ $errors->first('password') }}</p>
                    @endif
                    </div>
                </form>
            </div>
        </div>
    @endsection
