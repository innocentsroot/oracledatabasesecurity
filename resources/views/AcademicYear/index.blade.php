@extends('layouts.Semantic.app')
@section('content')

    <div class="ui unordered horizontal list">
        <div class="item">
            <button class="ui red button">
                <i class="file pdf outline icon"></i>
                Imprimer
            </button>
            <button class="ui green button">
                <i class="file excel outline icon"></i>
                Excel
            </button>
            <button class="ui blue button">
                <i class="file csv outline icon"></i>
                CSV
            </button>
        </div>
    </div>

    <div class="table-container" style="margin-top:54px;">

        <table id="clients-table" class="ui very basic table" >
            <thead>
            <tr>
                <th class="th-sm">
                    id Annee
                </th>
                <th class="th-sm">
                    Date de d&eacute;but
                    <i class="fa fa-sort float-right" aria-hidden="true"></i>
                </th>
                <th class="th-sm">
                    Date de fin
                    <i class="fa fa-sort float-right" aria-hidden="true"></i>
                </th>
                <th class="th-sm">
                    Nombre de niveau
                    <i class="fa fa-sort float-right" aria-hidden="true"></i>
                </th>
                <th class="th-sm">
                    Etat
                    <i class="fa fa-sort float-right" aria-hidden="true"></i>
                </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="bold">1</td>
                    <td class="bold">12-20-2019</td>
                    <td class="bold">12-20-2019</td>
                    <td>2</td>
                    <td style="white-space: nowrap; width: 2%;">
                        <button class="ui green button disable" id="disable">
                            <i class="thumbs up icon"></i>
                            En cours
                        </button>
                    </td>
                </tr>
                <tr>
                    <td class="bold">1</td>
                    <td class="bold">12-20-2019</td>
                    <td class="bold">12-20-2019</td>
                    <td>2</td>
                    <td style="white-space: nowrap; width: 2%;">
                        <button class="ui red button disable" id="disable">
                            <i class="exclamation triangle icon"></i>
                            Terminer
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <style>
        .card{
            /*DSIMiai$@togo*/
            padding:10px;
        }
        section.content {
            margin: 80px 15px 0 315px;
            -moz-transition: 0.5s;
            -o-transition: 0.5s;
            -webkit-transition: 0.5s;
            transition: 0.5s;
        }

        .btn{
            text-transform: none;
        }

        #disable{
            font-weight: bold;
        }

        .bold{
            font-weight: 900;
        }

        .ui.table{
            text-align: center;
        }
    </style>

@endsection
