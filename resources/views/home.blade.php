@extends('layouts.Semantic.app')
@section('content')
    <p>you are {{Auth::user() ? Auth::user() : 'not known :('}}</p>

    <div class="ui segment char-container" >
        <div class="chart" id="chart1"></div>
    </div>
    <div class="charts">
        <div class="ui segment char-container" >
            <div class="chart" id="chart2"></div>
        </div>

        <div class="ui segment char-container" style="margin-top: 0;">
            <div class="chart" id="chart3"></div>
        </div>

    </div>
@endsection

@section('other-script')
    <script src="{{asset("js/Charts/admin/autoSync.js")}}"></script>
    <script src="{{asset("js/jquery.sameheight.js")}}"></script>
    <script src="{{asset('js/Charts/apexcharts.min.js')}}"></script>
    <script src="{{asset('js/Charts/series.js')}}"></script>
    <script>
      $('.char-container').sameHeight();
      let chart1 = new ApexCharts(
        document.querySelector("#chart1"),
        {
          chart: {
            height: 380,
            type: 'line',
            zoom: {
              enabled: false
            }
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            curve: 'straight'
          },
          series: [{
            name: "Desktops",
            data: [30, 41, 35, 51, 49, 62, 69, 91, 126]
          }],
          title: {
            text: 'Product Trends by Month',
            align: 'left'
          },
          grid: {
            row: {
              colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
              opacity: 0.5
            },
          },
          labels: series.monthDataSeries1.dates,
          xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
          },
        }
      );

      let chart2 = new ApexCharts(
        document.querySelector("#chart2"),
        {
          chart: {
            height: 380,
            type: 'radialBar',
          },
          plotOptions: {
            radialBar: {
              hollow: {
                size: '70%',
              }
            },
          },
          series: [70],
          labels: ['Notes validees'],

        }
      );
      let chart3 = new ApexCharts(
        document.querySelector("#chart3"),
        {
          chart: {
            width: 380,
            type: 'pie',
          },
          series: [44, 55, 41, 17, 15],

        });

      chart3.render();
      chart1.render();
      chart2.render();
    </script>
@endsection

<style>
    .apexcharts-tooltip{
        border-radius: 2px !important;
    }

    .apexcharts-xaxistooltip-bottom::before {
        border-bottom-color: rgba(0,0,255,0.3);
    }

    .apexcharts-xaxistooltip {
        color: rgba(0,0,255,0.5) !important;
        font-weight: bold !important;
        background: #FFF !important;
        border: 2px solid #5FA1CB !important;
    }

    .apexcharts-tooltip.light .apexcharts-tooltip-title {
        background: #227dc7 !important;
        border-bottom: 1px solid #ddd !important;
        color: #FFF !important;
        font-weight: bold !important;
    }

    .charts{
        display: grid;
        grid-template-columns: 2fr 2fr;
        grid-column-gap: 5px;
    }

    .image-placeholder{
        vertical-align: middle;
    }

    @media only screen and (max-width: 1100px) {
        .charts {
            grid-template-columns: 1fr;
        }

    }

    #chart {
        max-width: 650px;
        margin: 35px auto;
    }

    #chart .apexcharts-xaxis-label {
        font-weight: bold;
    }

    .ui.menu .active.item {
        background: #FFF;
    }

    #image-placeholder-container{
        margin-left: auto;
        margin-right: auto;
        margin-top: 200px;
        vertical-align: center;
        display: none;
    }
</style>
