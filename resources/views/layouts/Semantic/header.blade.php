<div class="ui large top fixed menu  transition visible" id="menu" style="display: flex !important;">
    {{--<a href="" class="item">--}}
        {{--#--}}
    {{--</a>--}}

    <div class="ui container">
        <a class="active item" href="#">
            <i class="dashboard icon"></i>
            DashBoard
        </a>
        <a class="item" href="{{Route('academicYear.index')}}">
            <i class="calendar icon"></i>
            Academic year
        </a>
        <a class="item" href="{{Route('module.index')}}">
            <i class="recycle icon"></i>
            Modules
        </a>
        <a class="item" href="{{Route('student.index')}}">
            <i class="student icon"></i>
            Students
        </a>
        <a class="item" href="{{Route('teacher.index')}}">
            <i class="book icon"></i>
            Teacher
        </a>
        <a class="item" href="{{Route('level.index')}}">
            <i class="hourglass start icon"></i>
            Level
        </a>
        <a class="item" href="{{Route('function.index')}}">
            <i class="money icon"></i>
            Fonction
        </a>
        <a class="item" href="{{Route('serverStatus')}}">
            <i class="server icon"></i>
            Server
        </a>
        <div class="right menu">
            <div class=" item">
                <span class="firm">
                    HI-TECH
                     <i class="arrow down  icon"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="ui vertical inverted sidebar menu left">
    <a class="active item">Home</a>
    <a class="item">Work</a>
    <a class="item">Company</a>
    <a class="item">Careers</a>
    <a class="item">Login</a>
    <a class="item">Signup</a>
</div>

<style>
    .user-avatar{
        height: 40px;
        width: 40px;
        border-radius: 50%;
        vertical-align: middle;
        margin-top: 1px;
        /*border: 2px solid #aacc;*/
    }

    .ui.menu .active.item {
        border-bottom: 3px solid blue;
    }

    .ui.menu .item::before {
        background:none #FFF;
    }

    .firm{
        margin-left:4px;
    }

    .ui.menu.fixed {

        position:absolute;
        z-index: 10;
        margin: 0;
        width: 100%;
    }

    /*.ui.fixed.menu, .ui[class*="top fixed"].menu{*/
        /*top:0;*/
    /*}*/


</style>

@section('title')
    <p>Online Market</p>
@endsection
