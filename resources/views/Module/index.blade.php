@extends('layouts.Semantic.app')
@section('content')

    <div class="addForm ui grid" >
        <form action="{{Route('academicYear.store')}}" method="POST" class="ui form six wide  column centered">
            {{csrf_field()}}
            <div class="field">
                <label for="nomCategorie">Date de d&eacute;but</label>
                <select class="ui dropdown loading">
                    <option disabled>Veuillez s&eacute;lectionnez le niveau</option>
                    <option value="0">Niveau 1</option>
                    <option value="1">Niveau 2</option>
                    <option value="2">Niveau 3</option>
                </select>
                @if ($errors->has('nomCategorie'))
                    <span class="invalid-feedback" role="alert">
                            {{ $errors->first('nomCategorie') }}
                        </span>
                @endif
            </div>
        </form>

        <table id="clients-table" class="ui very basic table" >
            <thead>
            <tr>
                <th class="th-sm">
                    CodeModule
                </th>
                <th class="th-sm">
                    Nom Module
                    <i class="fa fa-sort float-right" aria-hidden="true"></i>
                </th>
                <th class="th-sm">
                    Volume Horaire
                    <i class="fa fa-sort float-right" aria-hidden="true"></i>
                </th>
                <th class="th-sm">
                    Professeurs
                    <i class="fa fa-sort float-right" aria-hidden="true"></i>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="bold">Modc41</td>
                <td class="bold">Securite des donnees</td>
                <td class="bold">24h</td>
                <td class="bold">Ali Mizou</td>
            </tr>
            <tr>
                <td class="bold">Modc41</td>
                <td class="bold">12-20-2019</td>
                <td class="bold">12-20-2019</td>
                <td>2</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

<style>
    .clients-table{
        margin-top:41px;
    }
</style>
