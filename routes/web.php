<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

/*Route::get('/{any}', 'SinglePageController@index')
    -> where('any', '.*');
*/
Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@redirectToDashboard')
    ->name('home');

/*Route::get('/academicYear', 'AcademicYearController@show')
    ->name('academicYear')
    ->middleware('auth.basic');
*/
Route::resource('academicYear', 'AcademicYearController');

Route::resource('module', 'ModuleController');
Route::resource('level', 'LevelController');
Route::resource('mark', 'MarkController');
Route::resource('student', 'StudentController');
Route::resource('teacher', 'TeacherController');
Route::resource('function', 'FunctionController');
Route::get('/server', 'ServerController@getStatus')
    ->name("serverStatus");

/*Route::get('/dashboard', 'DashboardController@redirectToDashboard')
    ->name('dashboard')
    ->middleware('auth.basic');
*/



Route::get("/addYear", function () {
    return 'Hello world';
});

Auth::routes();


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
