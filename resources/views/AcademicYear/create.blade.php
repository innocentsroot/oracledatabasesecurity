@extends('layouts.Semantic.app')
@section('content')
    <div class="addForm ui grid" >
        <form action="{{Route('academicYear.store')}}" method="POST" class="ui form seven wide  column centered">
            {{csrf_field()}}
            <div class="field">
                <label for="nomCategorie">Date de d&eacute;but</label>
                <input type="date" id="nomCategorie" name="nomCategorie">
                @if ($errors->has('nomCategorie'))
                    <span class="invalid-feedback" role="alert">
                        {{ $errors->first('nomCategorie') }}
                    </span>
                @endif
            </div>
            <div class="field">
                <label for="nomCategorie">Date de fin</label>
                <input type="date" id="nomCategorie" name="nomCategorie">
                @if ($errors->has('nomCategorie'))
                    <span class="invalid-feedback" role="alert">
                        {{ $errors->first('nomCategorie') }}
                    </span>
                @endif
            </div>
            <div class="m-t">
                <button type="submit" class="ui blue labeled submit icon button">
                    <i class="icon add"></i>
                    Ajouter
                </button>
                <button type="submit" class="ui red labeled submit icon button">
                    <i class="close icon"></i>
                    Annuler
                </button>
            </div>

        </form>
    </div>

@endsection


<style>
    .ui.form .field>label{
        text-align: left;
        font-weight: bold;
    }

    .ui.form .field {
        clear: both !important;
        margin: 0 0 2em !important;
    }

    .m-t{
        margin-top:50px;
    }
</style>
