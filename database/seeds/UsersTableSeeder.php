<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        for ($i = 0; $i <= 5; $i++) {
            DB::table('USERS')
                ->insert([
                    "password" => ".".bcrypt("root@dev")."",
                    "email" => "root@dev".$i,
                    "idFonction" => 2
                ]);
        }
    }
}
