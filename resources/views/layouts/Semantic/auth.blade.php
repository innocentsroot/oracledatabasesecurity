<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>StudentManager</title>
    <link rel="stylesheet" href="{{asset('css/semantic.css')}}">
    <script src="{{ asset('js/jquery.js') }}"></script>
    <style type="text/css">

        body {
            background-color: #FFF;
        }
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }

    </style>

</head>
<body>
@yield('content')
<script src="{{ asset('js/semantic.min.js') }}" ></script>
</body>
</html>
