<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Database Security</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/semantic.css')}}">
    <link rel="stylesheet" href="{{asset('/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('/css/loader.css')}}">
    {{--<link rel="stylesheet" href="{{asset('/css/admin/admin.css')}}">--}}
    <link rel="stylesheet" href="{{asset('/css/pace-white.css')}}">
    <script type="text/javascript" src={{asset("js/jquery.js")}}></script>
    @yield('others-css')

</head>
<body>
@include('layouts.Semantic.site')
@include('layouts.Semantic.header')

<div class="ui vertical stripe"  style="padding-top: 8rem;">
    <div class="ui middle aligned stackable grid container">
        <div class="row">
            <div class="wide column">
                @yield('content')
            </div>
        </div>
    </div>
</div>
@yield('other-script')
@include('layouts.Semantic.script')
</body>
</html>

<style>

    body{
        font-family: FontAwesome, "Open Sans", sans-serif;
    }

    .login-header-bar {
        width: 100%;
    }

    .site-container{
        padding-top:8rem;
    }

    .invalid-feedback{
        display: block;
    }

    .double-nav a{
        font-size: 1.25rem;
    }
    .double-nav a:hover{
        color:#FFF;
    }
    .navbar-brand-link{
        font-size: 1.25rem;
    }

    .ui.fixed.menu, .ui[class*="top fixed"].menu {
        top: auto;
        left: 0;
        right: auto;
        bottom: auto;
    }

    .site-name-wrapper{
        background-color: #0077c8;
    }

    .site-name{
        font-size: 2rem;
        line-height: 2.625rem;
        font-weight: 300;
        padding: 25px 10px 13px 20px;
        margin: 0;
        color: #fff;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        text-align: left;
    }
    .ui.menu:not(.secondary):not(.text):not(.tabular):not(.borderless) >
    .container >
    .item:not(.right):not(.borderless):first-child {
        border-left: none;
    }

    .ui.menu .active.item {
        background: #FFF;
    }
    .site-name a, a:hover{
        color:#FFF;
        font-weight: 400
    }

    .ui.form .field>label{
        text-align: left;
        font-weight: bold;
    }

    .ui.form .field {
        clear: both !important;
        margin: 0 0 2em !important;
    }
    .bold{
        font-weight: bold;
    }
</style>

<script>
    (function(){

        let presentation = document.querySelector('#site-presentation');
        let presentationHeigth = presentation.getBoundingClientRect().height;
        let menu = document.querySelector('#menu');
        let onScroll = function() {
            let scrolled = window.scrollY;
            if (scrolled > presentationHeigth) {
                console.log( 'On leve le menu' );
                menu.style.top = '0';
                $( '.ui.menu.fixed' ).css( 'position', 'fixed');
            }else{
                console.log( 'On descend le menu' );
                menu.style.top = 'auto';
                $( '.ui.menu.fixed' ).css( 'position', 'absolute');
//                presentation.fadeOut(1000);
            }
        };
        window.addEventListener('scroll', onScroll);

    })();
</script>
