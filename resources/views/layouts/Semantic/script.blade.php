<script type="text/javascript" src={{asset("js/semantic.min.js")}}></script>
<script type="text/javascript" src="{{asset('/js/jquery.nice-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/offline.min.js')}}"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.semanticui.js')}}"></script>
<script>
//    (function () {
//        $(document).ready(function() {
//            $('select').niceSelect();
//        });
//    })();

    $(document).ready(function () {
        $('#clients-table').DataTable({
            language:{
                "url": "{{asset('js/Json/lang.json')}}"
            }
        });
    });

$('.message .close')
    .on('click', function() {
        $(this)
            .closest('.message')
            .transition('fade')
        ;
    })
;


    $('.ui.dropdown').dropdown();

    $('.menu .item').tab();
</script>
<script type="text/javascript" src="{{asset('/js/pace.min.js')}}"></script>





