$(document).ready(function () {

    // $('#chart-data').on('click',function () {

        let $full = window.location.host.concat('/products-categories-chart-ajax');

        let $data = [1,2];

        $('#chart2').css('display', 'none');
        $('#image-placeholder-container').css('display', 'block');


        $.ajax({
            url: '/products-categories-chart-ajax',
            dataType: "json",
            method: "post",
            data: {
                "id": $data,
            }
        })
            .done(function (data){

                // let computed = Array.parseJSON(data);
                $('#image-placeholder-container').css('display', 'none');
                $('#chart2').css('display', 'block');
                let series = [];
                let categories = [];

                console.log(data.length);

                for(let i =0; i < data.length; i++){

                    if(data[i].categories){
                        categories.push(data[i].categories)
                    }
                    if(data[i].number){
                        series.push(data[i].number)
                    }
                }
                designChart1(series, categories);
            })
            .fail(function () {

            })

    });

    function designChart1(series, categories) {
        (function($){
            let colors = ['#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0', '#546E7A', '#26a69a', '#D10CE8'];

            let options = {
                annotations: {
                },
                chart: {
                    type: "histogram",
                    height: 380,
                    foreColor: "#999",
                    scroller: {
                        enabled: true,
                        thumb: {
                            background: '#00D8B6',
                        }
                    },
                    events: {
                        dataPointSelection: function(e, chart,opts) {
                            let arraySelected = [];
                            opts.selectedDataPoints.map((selectedIndex) => {
                                selectedIndex.map((s) => {
                                    arraySelected.push(chart.w.globals.series[0][s])
                                })

                            });
                            arraySelected = arraySelected.reduce((acc, curr) => {
                                return acc + curr;
                            }, 0);

                            document.querySelector("#selected-count").innerHTML = arraySelected
                        }
                    }
                },
                plotOptions: {
                    bar: {
                        distributed: true,
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
                series: [{
                    data: series
                }],
                title: {
                    text: 'Courbe 2: Produit par categorie'
                },
                states: {
                    active: {
                        allowMultipleDataPointsSelection: false
                    }
                },
                markers: {
                    size: 5,
                    strokeColor: "#fff",
                    strokeWidth: 3,
                    strokeOpacity: 1,
                    fillOpacity: 1,
                    hover: {
                        size: 8
                    }
                },
                xaxis: {
                    categories: categories,
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false
                    }
                },
                yaxis: {
                    tickAmount: 4,
                    labels: {
                        offsetX: -5,
                        offsetY: -5
                    },
                },

                tooltip: {
                    x: {
                        format: "dd MMM yyyy"
                    },
                },
            };

            let chart2 = new ApexCharts(
                document.querySelector("#chart2"),
                options
            );
            chart2.render();
        })();

    }


// });
