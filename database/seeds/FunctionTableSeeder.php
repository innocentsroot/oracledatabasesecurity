<?php

use Illuminate\Database\Seeder;
use App\UserFunction;
use Illuminate\Support\Facades\DB;
class FunctionTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('Fonction')
            ->insert([
                "idFonction" => 1,
                "libelleFonction" => "DASS"
            ]);
        DB::table('Fonction')
            ->insert([
                "idFonction" => 2,
                "libelleFonction" => "Enseignant"
            ]);
    }
}
